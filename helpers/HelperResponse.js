class HelperResponse{
    constructor() {
        this.config = require("../config/Config")
        this.logger = require("./Logger.js")
    }

     /**
     *
     * make call logs in development mode
     *
     * @param  {string/Object}  text
     * @return append log in file
     */
      callLog(text) {
        this.logger.logCall(text)
    }

   

    /**
     *
     * handle errors
     *
     * @param  {object}  response
     * @return {object}  error response
     */
     onError(res, err, title = "", isMessage = 0, type = 0) {
        this.logger.logError(title, err)
        var errMessage = "Server Error"

        if (err && err.message != undefined) {
            errMessage = err.message
        }
        if (isMessage == 1) {
            errMessage = err
        }
        if (type) {
            return res.status(400).json({ message: errMessage })
        } else {
             return res.status(400)
        }
    }

    /**
     *
     * handle success
     *
     * @param  {object}  response
     * @return {object}  success response
     */
     onSuccess(res, msg, type = 0) {
        if (type) {
            return res.status(200).json({ message: msg })
        } else {
            return res.status(200)
        }

    }

    /**
     *
     * get session
     *
     * @param  {string}  key
     * @return {object}
     */
     getSession(req, key) {
        return req.session[key]
    }

    /**
     *
     * set session
     *
     * @param  {string}  key
     * @param  {object}  value
     * @return {object}
     */
     setSession(req, key, value) {
        req.session[key] = value
    }

    /**
     *
     * detroy session
     * @param  {object}  request
     * @return {object}
     */
     destroySession(req) {
        req.session.destroy()
    }
}

module.exports = new HelperResponse()