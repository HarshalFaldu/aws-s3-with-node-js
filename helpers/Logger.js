class Logger{
    constructor() {
        this.fs = require('fs')
        this.config = require('../config/Config')
        if(!this.fs.existsSync("logs")){
            this.fs.mkdirSync("logs")
        }
    }
    
    /**
     *
     * log error
     *
     * @param  {string/Object}  text
     * @return append log in file
     */
     logError(title, text) {
        var errLog = "Function/API Endpoint:" + title + "\n" + JSON.stringify(text) + "\n ============================== \n"
        this.fs.appendFile("logs/error.log", errLog, function (err) { })
    }

     /**
     *
     * log error
     *
     * @param  {string/Object}  text
     * @return append log in file
     */
      logCall(text) {
        this.fs.appendFile("logs/calls.log", JSON.stringify(text) + "\n ============================== \n", function (err) { })
    }
}

module.exports = new Logger()