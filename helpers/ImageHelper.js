class ImageHelper{
    constructor(){
        this.AWS = require('aws-sdk')
        this.AWS.config.update({region: process.env.AWS_REGION});

        this.s3 = new this.AWS.S3();

        
    }


    getImageList(){
        return new Promise((resolve,reject) => {
            var bucketParams = {
                Bucket : process.env.AWS_BUCKET_NAME,
            };
    
             this.s3.listObjects(bucketParams, function(err, data) {
                if (err) {
                  console.log("Error", err);
                } else {
                  console.log("Success");
                  resolve(data)
                }
            });
        })
    }
}

module.exports = new ImageHelper()