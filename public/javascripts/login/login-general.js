
// Class Definition
var KTLogin = function() {
    var _login;

    var _showForm = function(form) {
        var cls = 'login-' + form + '-on';
        var form = 'kt_login_' + form + '_form';

        _login.removeClass('login-signin-on');

        _login.addClass(cls);

        KTUtil.animateClass(KTUtil.getById(form), 'animate__animated animate__backInUp');
    }

    var _handleSignInForm = function() {
        var validation;

        // Init form validation rules. For more info check the FormValidation plugin's official documentation:https://formvalidation.io/
        validation = FormValidation.formValidation(
			KTUtil.getById('kt_login_signin_form'),
			{
				fields: {
					username: {
						validators: {
							notEmpty: {
								message: 'Username is required'
							}
						}
					},
					password: {
						validators: {
							notEmpty: {
								message: 'Password is required'
							}
						}
					}
				},
				plugins: {
                    trigger: new FormValidation.plugins.Trigger(),
                    submitButton: new FormValidation.plugins.SubmitButton(),
                    //defaultSubmit: new FormValidation.plugins.DefaultSubmit(), // Uncomment this line to enable normal button submit after form validation
					bootstrap: new FormValidation.plugins.Bootstrap()
				}
			}
		);

		//  After submission
        $(document).on('click','#kt_login_signin_submit', function () {
			validation.validate().then(function(status) {
				if (status == 'Valid') {
					apiData = {
						employeeID: $("#username").val(),
						password:$("#password").val()
					}
					Api.post("api/login_user",null, apiData, function (error,res) {
						if(error){
							if(error.status == false){
								swal.fire({
									text: error.error.message,//error.error.message=="\"email\" must be a valid email" ? "Email must be a Valid Email" : error.error.message,
									icon: "error",
									buttonsStyling: false,
									confirmButtonText: "Ok, got it!",
									customClass: {
										confirmButton: "btn font-weight-bold btn-light-primary"
									}
								}).then(function() {
									KTUtil.scrollTop();
								});
						
							}
						}
						else{
							window.location.href = "/index"
						}
					})
                    
				} else {
					swal.fire({
		                text: "Please Enter Username or Password",
		                icon: "error",
		                buttonsStyling: false,
		                confirmButtonText: "Ok, got it!",
                        customClass: {
    						confirmButton: "btn font-weight-bold btn-light-primary"
    					}
		            }).then(function() {
						KTUtil.scrollTop();
					});
				}
		    });
        });

        // Handle forgot button
        $('#kt_login_forgot').on('click', function (e) {
            e.preventDefault();
            _showForm('forgot');
        });
    }

    

    // Public Functions
    return {
        // public functions
        init: function() {
            _login = $('#kt_login');

            _handleSignInForm();
        }
    };
}();

// Class Initialization
jQuery(document).ready(function() {

	// Check for device
	// function detectMob() {
	// 	const toMatch = [
	// 		/Android/i,
	// 		/webOS/i,
	// 		/iPhone/i,
	// 		/iPad/i,
	// 		/iPod/i,
	// 		/BlackBerry/i,
	// 		/Windows Phone/i
	// 	];
	
	// 	return toMatch.some((toMatchItem) => {
	// 		return navigator.userAgent.match(toMatchItem);
	// 	});
	// }

	// if(detectMob()==true){
	// 	$(".isMobile").removeClass("hiddenclass")
	// 	$(".deviceCheck").removeClass("d-flex").addClass("hiddenclass")
	// 	swal.fire("Use Computer device only")

	// }else{
	// }

	// $.get("https://api.ipdata.co?api-key=e7b970697256cca2506a4e1dfe8e6332d6b57f819bd4d015f2757f2f", function(response) {
	// 	console.log("IP JS: ",response);
	// }, "jsonp");
	KTLogin.init();

});
