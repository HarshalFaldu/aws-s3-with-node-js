const BaseController = require('./BaseController')
const config = require('../config/Config')

class AuthController extends BaseController{
    constructor() {
        super()
        this.es6BindAll = require("es6bindall");
        this.es6BindAll(this, ["login", "login_user", "logout"]);

        this.loginValidation = this.Joi.object({
            password: this.Joi.string().required(),
            employeeID: this.Joi.string().required(),
        });
    }

    /**
   *
   * login
   *
   * @param  {object}   request
   * @param  {object}   response
   * @return {object}	  login
   */
    async login(req, res){
      res.render("login")
    }


    /**
   *
   * login_user
   *
   * @param  {object}   request
   * @param  {object}   response
   * @return {object}	  login_user
   */
  async login_user(req, res) {
    var validationRes = this.loginValidation.validate(req.body);
    if (validationRes.error) {
      res.status(400).send({
        message: validationRes.error.details[0].message,
        type: "ValidationError",
      });
    } else {
      if(req.body.employeeID == process.env.EMAIL_ID && req.body.password == process.env.PASSWORD){
        this.helperResponse.setSession(req, "user",req.body.employeeID);
        res.send({status: true})
      }
      else {
        res.status(400).send({
          message: config.messages.INVALID_CREDENTIALS,
          type: "ValidationError",
        });
      }
    }
  }


  /**
   *
   * used for logout
   *
   * @param  {object}   request
   * @param  {object}   response
   * @return {object}	 redirect to home page
   */
   logout(req, res) {
    this.helperResponse.destroySession(req);
    res.redirect("/");
  }


}

module.exports = new AuthController();