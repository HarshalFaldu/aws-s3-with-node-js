const BaseController = require("./BaseController");
class DashboardController extends BaseController {
  constructor() {
    super();
    this.es6BindAll = require("es6bindall");
    this.imageHelper = require("../helpers/ImageHelper")
      this.es6BindAll(this, ["home","error","errorNotFound"]);
    }

    /**
    *
    * home page
    *
    * @param  {object}   request
    * @param  {object}   response
    * @return {view}
    */
    async home(req, res) {
        var data = await this.imageHelper.getImageList()
        var imageNames = []
        for (let i = 0; i < data.Contents.length; i++) {
          const element = data.Contents[i];
          imageNames.push(element.Key)
        }
        res.render("index",{
            layout: "layout/layout",
            title: "dashboard",
            data : JSON.stringify(imageNames),
            bucket_name : process.env.AWS_BUCKET_NAME,
            aws_region : process.env.AWS_REGION
        });
    }

     /**
    *
    * Error page
    *
    * @param  {object}   request
    * @param  {object}   response
    * @return {view}
    */
      error(req, res) {
        res.render("error",{
            message: "Not Authorized",
            title: "none",
            status:401
        });
    }
    
    
    /**
        *
        * Error page
        *
        * @param  {object}   request
        * @param  {object}   response
        * @return {view}
        */
     errorNotFound(req, res) {
      res.render("error",{
          message: "Page Not Found",
          title: "none",
          status:404
      });
    }
}

module.exports = new DashboardController()