/**
 * This class will be extended by all other controllers
 */
class BaseController {
    constructor() {
        this.mongoose = require('mongoose')
        this.Joi = require('joi')
        this.bcrypt = require('bcrypt')
        this.helperResponse = require('../helpers/HelperResponse')
    }
}

module.exports = BaseController;